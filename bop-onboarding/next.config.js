const { NextFederationPlugin } = require("@module-federation/nextjs-mf");

module.exports = {
  webpack(config, options) {
    const { isServer } = options;
    config.plugins.push(
      new NextFederationPlugin({
        name: "onboarding",
        remotes: {},
        filename: "static/chunks/remoteEntry.js",
        exposes: {
          "./nextjs-remote-component":
            "./components/nextjs-remote-component.js",
          "./cdd": "./src/pages/cdd/index.tsx",
        },
        shared: {
          // whatever else
        },
      })
    );

    return config;
  },
};
