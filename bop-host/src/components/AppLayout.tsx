import Link from "next/link";
import React, { ReactNode } from "react";

type PageProps = {
  children: ReactNode;
};

export default function AppLayout(props: PageProps) {
  return (
    <div>
      <div style={{ display: "flex", padding: "20px 0px" }}>
        <Link href="/">
          <div style={{ padding: "0px 20px", cursor: "pointer" }}>Home</div>
        </Link>
        <Link href="/customer-info">
          <div style={{ padding: "0px 20px", cursor: "pointer" }}>
            Customer Info
          </div>
        </Link>
        <Link href="/cdd">
          <div style={{ padding: "0px 20px", cursor: "pointer" }}>Cdd</div>
        </Link>
      </div>
      <div>{props.children}</div>
    </div>
  );
}
