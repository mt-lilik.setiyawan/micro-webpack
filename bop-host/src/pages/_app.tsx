import AppLayout from "@/components/AppLayout";
import "@/styles/globals.css";
import type { AppProps } from "next/app";
import { useRouter } from "next/router";
import { useEffect } from "react";
import { useCookies } from "react-cookie";

export default function App({ Component, pageProps }: AppProps) {
  const navigate = useRouter();
  const [cookies, setCookie] = useCookies(["accessToken"]);

  useEffect(() => {
    if (!cookies.accessToken) {
      navigate.push("/login");
    }
  }, []);
  return (
    <AppLayout>
      <Component {...pageProps} />
    </AppLayout>
  );
}
