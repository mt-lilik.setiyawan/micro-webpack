import NextImport from "../../components/nextjs-remote-component";

export default function Home() {
  return (
    <div>
      <div>Index onboarding</div>
      <div>
        <NextImport />
      </div>
    </div>
  );
}
