import { useEffect, useState } from "react";
import { useCookies } from "react-cookie";

export default function Cdd() {
  const [accessToken, setAccessToken] = useState("");
  const [cookies, setCookie] = useCookies(["accessToken"]);

  useEffect(() => {
    setAccessToken(cookies.accessToken);
  }, []);

  return (
    <div>Cdd Screen from onboarding update with token : {accessToken}</div>
  );
}
