import dynamic from "next/dynamic";
import { useEffect } from "react";

const CddRemoteComponent = dynamic(() => import("onboarding/cdd"));

export default function CustomerInfo() {
  useEffect(() => {
    console.log("hello there");
  }, []);

  return (
    <div>
      <CddRemoteComponent />
    </div>
  );
}
