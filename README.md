This repository is a spike for microfornend for BOP. 

- The host will orchestrate the whole mfe, login functionality, and customer info screen, and task list. 
- The onboarding will provide the cdd / edd maker side. 

Tech stack 
- next js
- webpack module federation

How to Use
- clone the repo
- cd bop-host && npm install && npm run dev
- cd bop-onboarding && npm install && npm run dev
- go to localhost:3000 -> host
- go to localhost:8081 -> remote

================================

- need to know wether it is ssr or not
- Error: Text content does not match server-rendered HTML. : to resolve, need to redeploy both
