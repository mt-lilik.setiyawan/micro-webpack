import { useRouter } from "next/router";
import { useCookies } from "react-cookie";

export default function Login() {
  const navigate = useRouter();
  const [cookies, setCookie] = useCookies(["accessToken"]);

  const handleClick = () => {
    setCookie("accessToken", "123456789");
    navigate.push("/");
  };

  return (
    <div>
      <button onClick={handleClick}>Login</button>
    </div>
  );
}
