import dynamic from "next/dynamic";
import { useEffect } from "react";

const NextjsRemoteComponent = dynamic(
  () => import("onboarding/nextjs-remote-component")
);

export default function Home() {
  return (
    <div>
      <div>Index</div>
      <NextjsRemoteComponent />
    </div>
  );
}
